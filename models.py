from django.db import models

class House(models.Model):
    Address=models.TextField()
    pellak=models.IntegerField(unique=True)
    owner=models.ForeignKey(Users,on_delete=models.CASCADE)
    
class Countor(models.Model):
    House=models.ForeignKey(House,on_delete=models.CASCADE)
    owner=models.ForeignKey(Users,on_delete=models.CASCADE)


class Users(models.Model):
    name=models.CharField(max_length=20)
    codeM=models.IntegerField(unique=True)

class Payment(models.Model):
    Status=(
    ('N','not complet')
    ('Y','completed')
    amount=IntegerField(help_text='amount unit is Toman')
    peyment_for=models.ForeignKey(Countor,on_delete=models.CASCADE)
    Issuance_date=models.DateField()
    peyment_status=models.CharField(max_length=1,Choices=Status,default='N')